#! /usr/bin/env bash
#El programa revisa la existencia de una carpeta y un archivo, si existen abrimos el archivo:
ls ~ -lRS | grep "^d"> Directorio.txt #como unicamente quiero directorios utilizo el "^d", solo los documentos que empiezan con d se muestran (directories)
echo "Escriba el nombre EXACTO del directorio que quiere verificar si existe:"
read DIR
G=$(grep -wq $DIR Directorio.txt ; echo $?) #hacemos un grep de EXACTAMENTE (-w) la palabra que enviamos, no queremos el output (-q), echo $? responde 0 si se ejecuto el grep 1 si no se ejecuto

if [ "$G" == "0" ] #proceso donde evaluamos la informacion que recien almacenamos, G-existe? G2-permiso?
	then
		echo "EL DIRECTOEIO EXISTE"
		path=$(find $HOME -type d -name $DIR)
	else
		echo "EL DIRECTORIO NO EXISTE"
fi

cd path
ls
echo "Cual de los archivos mostrados quiere abrir? " 
read FILE

##Revision de existencia y apertura en caso que exista
if [ -f $FILE ] ##revisar si busca en el mismo directorio o toca dar el path, si es asi agregar antes FILE_2=path/FILE
	then
   	  	xdg-open $FILE
	else
        echo Verifique si el archivo que escribio concuerda con la lista mostrada:
        ls
fi
